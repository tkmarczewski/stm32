/**
  ******************************************************************************
  * File Name          : l3gd20.c
  * Description        : This file provides driver code for gyroscope
  *
  ******************************************************************************
*/

#include "l3gd20.h"
#include "stm32f3xx.h"
#include "spi.h"
#include "usart.h"
