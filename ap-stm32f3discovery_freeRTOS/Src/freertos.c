#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"


osThreadId defaultTaskHandle;
osThreadId gyroCheckTaskHandle;

void StartDefaultTask(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

void StartGyroCheckTask(void const * argument);

void MX_FREERTOS_Init(void) {
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);
}


void StartDefaultTask(void const * argument)
{

  for(;;)
  {
    osDelay(100);
  }
}

